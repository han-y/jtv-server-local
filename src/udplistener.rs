use rocket::tokio::net::UdpSocket;

pub async fn bind_udp() -> std::io::Result<()> {
    let sock = UdpSocket::bind("0.0.0.0:37820").await?;
    let mut buf = [0; 1024];
    loop {
        let (len, addr) = sock.recv_from(&mut buf).await?;
        println!("{:?} bytes received from {:?}", len, addr);
        if let Some(request) = std::str::from_utf8(&buf[..len]).ok() {
            if request.starts_with("JTV LOCAL SERVER REQUEST") {
                let _ = sock.send_to("JTV LOCAL SERVER ACK".as_bytes(), addr).await?;
                println!("ACK Sent");
            } else {
                println!("Unknow request {:?}", request);
            }
        }
        buf = [0; 1024];
    }
}