#[macro_use]
extern crate rocket;
mod udplistener;
use rocket::serde::{json::Json, Deserialize, Serialize};
use rocket::{fairing::AdHoc, State};
use std::path::{Path, PathBuf};

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
struct StoragePath {
    pub root_dir: String,
    pub server_addr: String,
}
#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
struct FileObject {
    url: String,
    file_type: String,
    file_name: String,
}

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}
fn construct_file_object<S: AsRef<str>>(
    entry_result: std::io::Result<std::fs::DirEntry>,
    server_addr: &String,
    path: S,
) -> Option<FileObject> {
    let entry = entry_result.ok()?;
    let file_name = entry.file_name();
    let file_name = file_name.to_string_lossy();
    let url = if entry.file_type().ok()?.is_dir() {
        format!("{}/family/{}/{}", server_addr, path.as_ref(), file_name)
    } else {
        format!(
            "{}/storage/Family/{}/{}",
            server_addr,
            path.as_ref(),
            file_name
        )
    };
    let file_type = if entry.file_type().ok()?.is_dir() {
        "dir".to_string()
    } else {
        let file_name_lower = file_name.to_lowercase();

        if file_name_lower.ends_with(".jpg") {
            "image".to_string()
        } else if file_name_lower.ends_with(".mkv")
            || file_name_lower.ends_with(".mp4")
            || file_name_lower.ends_with(".webm")
        {
            "video".to_string()
        } else {
            "none".to_string()
        }
    };

    if file_type == "none" {
        return None;
    }
    let file_name = file_name.to_string();
    Some(FileObject {
        url,
        file_name,
        file_type,
    })
}
#[get("/family/<path..>")]
fn get_dir(path: PathBuf, storage_path: &State<StoragePath>) -> Option<Json<Vec<FileObject>>> {
    let root_dir: &Path = storage_path.root_dir.as_ref();

    let final_path = root_dir.join(path.clone());
    let mut result: Vec<FileObject> = vec![];
    for entry_result in final_path.read_dir().ok()? {
        match construct_file_object(
            entry_result,
            &storage_path.server_addr,
            path.to_string_lossy(),
        ) {
            Some(obj) => result.push(obj),
            None => {}
        }
    }
    Some(Json(result))
}

#[launch]
fn rocket() -> _ {
    let rocket = rocket::build();

    rocket::tokio::spawn(async move {
        let _ = udplistener::bind_udp().await;
    });

    rocket
        .mount("/", routes![index, get_dir])
        .attach(AdHoc::config::<StoragePath>())
}
